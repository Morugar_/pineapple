import json as js
import socket
import time as t
import requests as req

def send_frame(json):
    try:
        tcp.sendall(bytes(json, encoding="utf-8"))
    except Exception as error:
        print(error)
    else:
        print("0")

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

default_server = "localhost"
default_port = 4214

server = ''
port = 0

new_server = str(input('Type in server (enter for default)'))
new_port = str(input('Type in server (enter for default)'))
data_to_send = input("Enter data for sending: ")

size = 2048

if new_server:
    server = new_server
else:
    server = default_server

if new_port:
    port = new_port
else:
    port = default_port


try:
    print('Connetcting...')
    tcp.connect((server, port))
    t.sleep(1)
    data_to_send.encode(encoding="utf-8")
    send_frame(data_to_send)
except ConnectionRefusedError:
    print("Error while connecting.")
finally:
    print('Connection close.')
    tcp.close()
